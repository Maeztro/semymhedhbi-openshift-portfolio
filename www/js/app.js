(function (angular) {
    // "use strict";

    var appModule = angular.module('app',[
    //core
    'app.controllers',
    'app.constants',
    'app.services',
    'app.directives',
    'app.routing'
    //auth

]);

})(angular);

