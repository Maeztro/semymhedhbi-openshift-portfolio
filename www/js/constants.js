(function (angular) {
    // "use strict";

    var constantModule = angular.module('app.constants', []);
    // constantModule.constant('CONSTANT_NAME', 'https://');
    constantModule.constant('SERVER_PATH', 'http://localhost:3000/');

})(angular);