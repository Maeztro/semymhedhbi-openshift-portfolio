(function (angular) {
    // "use strict";

var serviceClientModule = angular.module('app.services',[]);

    serviceClientModule.factory('serviceGallery',[
        'SERVER_PATH',
        '$http',
        'serviceAuth',
        '$q'
        ,function(SERVER_PATH,$http,serviceAuth,$q){

    function get(uri, params){
        uri = uri.charAt(0) === '/' ? uri.substr(1) : uri;
        params = params || {};
        return $http({
            method: 'GET',
            url: SERVER_PATH + uri,
            params : params
        })
    }

    function post(uri, params){
        uri = uri.charAt(0) === '/' ? uri.substr(1) : uri;
        params = params || {};

        //console.log('from post',params);

        return $http({
            method: 'POST',
            url: SERVER_PATH + uri,
            data : params,
            headers:serviceAuth.getHeaders()//cause no 401 error
        })
    }

    function del(uri, params){
        uri = uri.charAt(0) === '/' ? uri.substr(1) : uri;
        params = params || {};
        return $http({
            method: 'DELETE',
            url: SERVER_PATH + uri,
            data : params,
            headers:serviceAuth.getHeaders()//cause no 401 error
        })
    }

    function put(uri, params){
        uri = uri.charAt(0) === '/' ? uri.substr(1) : uri;
        params = params || {};

        return $http({
            method: 'PUT',
            url: SERVER_PATH + uri,
            data : params,
            headers:serviceAuth.getHeaders()
        })
    }

//------------------------------------------------

    function getCardsList(){
        return get('/api/portfolio')
            .then(function(response){
                //console.log(response);
                return response.data.items;
            }, function(err){
                console.error(err);
                return [];
            });
    }

    function getCardById(id){
        return get('/api/portfolio/'+ id)
            .then(function(response){
                //console.log(response);
                return response.data.item;
            }, function(err){
                console.error(err);
                return [];
            });
    }

    function postCard(card){
        //console.log(card);
        return post('/api/portfolio/',card)
            .then(function(response){
                //console.log(response);
                return response.data.message;
            }, function(err){
                console.error(err);
                return $q.reject({message:err});
            });
    }

    function editCard(arg){
        //console.log('editing this:',arg);
        return put('/api/portfolio/'+ arg._id,arg)
            .then(function(response){
                //console.log(response);
                return response.data.message;
            }, function(err){
                console.error(err);
                //return $q.reject({message:err});
            });
    }

    function deleteCard(arg){
        //console.log(arg);
        return del('/api/portfolio/'+arg._id,arg)
            .then(function(response){
                //console.log(response);
                return response.data.message;
            }, function(err){
                console.error(err);
                return $q.reject({message:err});
            });
    }

    function getFormModel(){
        return {
                title:'',
                description:'',
                src:'',
                link:'',
                url:''
               }
    }

    return {
        getCardsList: getCardsList,
        getCardById: getCardById,
        postCard: postCard,
        getFormModel: getFormModel,
        editCard: editCard,
        deleteCard: deleteCard
    }
}]);

serviceClientModule.factory('serviceAuth',[
    '$rootScope',
    'SERVER_PATH',
    '$http',
    '$window',
    function($rootScope,SERVER_PATH,$http,$window){

    function get(uri, params){
        uri = uri.charAt(0) === '/' ? uri.substr(1) : uri;
        params = params || {};
        return $http({
            method: 'GET',
            url: SERVER_PATH + uri,
            params : params
        })
    }

    function post(uri, params){
        uri = uri.charAt(0) === '/' ? uri.substr(1) : uri;
        params = params || {};
        return $http({
            method: 'POST',
            url: SERVER_PATH + uri,
            data : params
        })
    }

    var authToken='sessionToken';

    var payload;

    function saveToken(token){
        $window.localStorage[authToken]=token;
    }

    function getToken(){
        return $window.localStorage[authToken];
    }

    function getHeaders(){
        return {Authorization:'Bearer '+ getToken()};
    }

    //-------------------------------

    var is_logged = isLogged();
    //console.log('on reload is_logged value is',is_logged);
    //console.log('on reload getLoggedStatus() returns',getLoggedStatus());

    function getLoggedStatus(){
        return is_logged;
    }

    function notify() {
        $rootScope.$emit('logged-status-changed');
    }

    function updateLoggedStatus(arg){
        is_logged = arg;
        notify();
    }

    function isLogged(){
        if(payload==null){
            var token=getToken();
            //console.log('on reload the token is',token);
            if(!token){
                return false;
            }
            payload=JSON.parse($window.atob(token.split('.')[1]));
            //console.log(payload);
        }
        //console.log('and its validity is ',payload.exp>Date.now()/1000);
        return payload.exp>Date.now()/1000;
    }

    function getLoggedUserId(){
        if(payload==null){
            var token=getToken();
            //console.log('on reload the token is',token);
            if(!token){
                return false;
            }
            payload=JSON.parse($window.atob(token.split('.')[1]));
        }
        return payload.user.id;
    }

    function login(credentials) {
        return post('api/users/login',credentials)
            .then(function(response){
                //console.log('recieved:',response.data);
                saveToken(response.data.token);
                updateLoggedStatus(true);
            }, function(err){
                //console.log(err);
                console.error(err);
                return [];
            });
    }

    function register(credentials) {
        //console.log(credentials);
        return post('api/users/register',credentials)
            .then(function(response){
                //console.log(response);
                updateLoggedStatus(true);
                return response.data;
            }, function(err){
                // console.log(err);
                console.error(err);
                return [];
            });
    }

    function logout() {
        $window.localStorage.clear();
        updateLoggedStatus(false);
    }

    function subscribe(scope, callback) {
        var handler = $rootScope.$on('logged-status-changed', callback);
        scope.$on('$destroy', handler);
    }

    return {
        getLoggedStatus: getLoggedStatus,
        updateLoggedStatus: updateLoggedStatus,
        login: login,
        logout: logout,
        register: register,
        subscribe: subscribe,
        getHeaders: getHeaders,
        getLoggedUserId: getLoggedUserId,
    }

}]);

})(angular);