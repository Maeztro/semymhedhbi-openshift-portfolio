(function (angular) {
    // "use strict";

    var directiveModule =  angular.module('app.directives',[]);

//MATERIALIZE---------------------
directiveModule.directive('matSidemenu',function(){
    return{
        scope:{
            element:'='
        },
        controller:function($scope){
            // Initialize collapse button
            $(".button-collapse").sideNav();
            // Initialize collapsible
            $('.collapsible').collapsible();
        },
        restrict:'E',
        templateUrl:'templates/sidemenu.html',
        replace:false,
        transclude:true
    };
});

directiveModule.directive('matCardimage',function(){
    return{
        restrict:'E',
        scope:{
            element:'='
        },
        controller:function($scope){
            $(document).ready(function(){
                $('.materialboxed').materialbox();
            });
        },
        templateUrl:'templates/cardimage.html',
        replace:false,
        transclude:true
    };
});

directiveModule.directive('matCardhorizontal',function(){
    return{
        restrict:'E',
        scope:{
            element:'='
        },
        controller:function($scope){},
        templateUrl:'templates/cardhorizontal.html',
        replace:false,
        transclude:true
    };
});

directiveModule.directive('matCardreveal',function(){
    return{
        restrict:'E',
        scope:{
            element:'='
        },
        controller:function($scope){
            $(document).ready(function(){
                $('.materialboxed').materialbox();
            });
        },
        templateUrl:'templates/cardreveal.html',
        replace:false,
        transclude:true
    };
});

directiveModule.directive('matForminput',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        templateUrl:'templates/forminput.html',
        replace:false,
        transclude:true
    };
});

directiveModule.directive('matFormsubmit',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        templateUrl:'templates/formsubmit.html',
        replace:false,
        transclude:true
    };
});

directiveModule.directive('matLogin',function(){
    return{
        restrict:'E',
        scope:{
            element:'='
        },
        controller:function($scope,serviceAuth){

            $scope.login_info = {username:'',password:''};

            $scope.is_logged = serviceAuth.getLoggedStatus();

            serviceAuth.subscribe($scope, function authStatusChanged() {
                $scope.is_logged =  serviceAuth.getLoggedStatus();
                //console.log('matLogin',$scope.is_logged);
            });

            function init(){
                $scope.is_logged =  serviceAuth.getLoggedStatus();
                //console.log('on init $scope.is_logged value is',$scope.is_logged,'in matLogin');
                //console.log('on init serviceAuth.getLoggedStatus() returns',serviceAuth.getLoggedStatus(),'in matLogin');
            }
            init();

            $scope.login = function(arg){
                //console.log('login',arg);
                serviceAuth.login(arg);
            };

        },
        controllerAs:'galleryCtrl',
        bindToController: true,
        templateUrl:'templates/login.html',
        replace:false,
        transclude:true
    };
});

    directiveModule.directive('matLogout',function(){
        return{
            restrict:'E',
            scope:{
                element:'='
            },
            controller:function($scope,serviceAuth){

                $scope.is_logged = serviceAuth.getLoggedStatus();

                serviceAuth.subscribe($scope, function authStatusChanged() {
                    $scope.is_logged =  serviceAuth.getLoggedStatus();
                    //console.log('matLogout',$scope.is_logged);
                });

                function init(){
                    $scope.is_logged =  serviceAuth.getLoggedStatus();
                    //console.log('on init $scope.is_logged value is',$scope.is_logged,'in matLogout');
                    //console.log('on init serviceAuth.getLoggedStatus() returns',serviceAuth.getLoggedStatus(),'in matLogout');
                }
                init();

                $scope.logout = function(){
                    serviceAuth.logout();
                };

            },
            controllerAs:'galleryCtrl',
            bindToController: true,
            templateUrl:'templates/logout.html',
            replace:false,
            transclude:true
        };
    });

    directiveModule.directive('matRegister',function(){
        return{
            restrict:'E',
            scope:{
                element:'='
            },
            controller:function($scope,serviceAuth){

                $scope.register_info = {name:'',email:'',username:'',password:''};

                $scope.is_logged = serviceAuth.getLoggedStatus();

                serviceAuth.subscribe($scope, function authStatusChanged() {
                    $scope.is_logged =  serviceAuth.getLoggedStatus();
                    //console.log('matRegister',$scope.is_logged);
                });

                function init(){
                    $scope.is_logged =  serviceAuth.getLoggedStatus();
                    //console.log('on init $scope.is_logged value is',$scope.is_logged,'in matRegister');
                    //console.log('on init serviceAuth.getLoggedStatus() returns',serviceAuth.getLoggedStatus(),'in matRegister');
                }
                init();

                $scope.register = function(arg){
                    //console.log('register',arg);
                    serviceAuth.register(arg);
                };

            },
            controllerAs:'galleryCtrl',
            bindToController: true,
            templateUrl:'templates/register.html',
            replace:false,
            transclude:true
        };
    });

})(angular);