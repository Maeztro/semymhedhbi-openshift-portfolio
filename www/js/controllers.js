(function (angular) {
    "use strict";

var controllerModule = angular.module('app.controllers', [])

    // home
    controllerModule.controller('homeCtrl', [
        '$scope',
        '$state',
        'serviceGallery',
        function ($scope, $state, serviceGallery) {

        }
    ]);

    //gallery
    controllerModule.controller('galleryCtrl', [
        '$scope',
        'serviceGallery',
        'serviceAuth',
        function ($scope, serviceGallery,serviceAuth) {
            angular.element(document).ready(function () {

            //init
            $scope.current_logged_user = '';

            $scope.is_logged = false;

            serviceAuth.subscribe($scope, function authStatusChanged() {
                $scope.is_logged =  serviceAuth.getLoggedStatus();
                //console.log('$scope.is_logged status is:',$scope.is_logged);
            });

            $scope.retrieveServerCards = function(){
                return serviceGallery.getCardsList()
                    .then(function(response){
                        $scope.card_list = response;
                       // console.log('card list is : ',$scope.card_list);
                        $scope.initForm();
                    })
            };

            function init(){
                $scope.is_logged =  serviceAuth.getLoggedStatus();
                $scope.current_logged_user = serviceAuth.getLoggedUserId();
                //console.log('on init $scope.is_logged value is',$scope.is_logged,'in galleryCtrl');
                //console.log('on init serviceAuth.getLoggedStatus() returns',serviceAuth.getLoggedStatus(),'in galleryCtrl');
            }
            init();

            //forms

            $scope.form_data = {};

            $scope.show_form = false;

            $scope.initForm = function(){
                $scope.form_data = serviceGallery.getFormModel();
                //console.log($scope.form_data);
                $scope.form_data.belongsto = $scope.current_logged_user;
            };
            $scope.initForm();

            $scope.isValid = function(arg){
                var result = false;
                if(arg != null && arg !=="" && arg !== undefined){
                    result = true;
                }
                return result;
            };

            $scope.testForm = function(obj){
                var test_form = false;
                if($scope.isValid(obj.title) && $scope.isValid(obj.description) && $scope.isValid(obj.src) && $scope.isValid(obj.url) && $scope.isValid(obj.link) && $scope.isValid(obj.category)){
                    test_form = true;
                }
                return test_form;
            };

            //display posts  //$scope.current_logged_user

            $scope.retrieveServerCards();

            $scope.post = function(obj){
                if( $scope.testForm(obj)){
                    serviceGallery.postCard(obj).then(function(){
                        $scope.retrieveServerCards();
                    });
                }else{
                    console.error('some fields may be invalid or empty');
                }
                $scope.show_form = false;
            };

            $scope.edit = function(arg){
                serviceGallery.editCard(arg);
                $scope.retrieveServerCards();
            };

            $scope.delete = function(arg){
                serviceGallery.deleteCard(arg);
                $scope.retrieveServerCards();
            };

            });
        }
    ]);

})(angular);
