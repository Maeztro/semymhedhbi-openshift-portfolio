(function (angular) {
    // "use strict";

    var routeModule = angular.module('app.routing',[
        'ui.router'
    ]);

    routeModule.config(function($stateProvider, $urlRouterProvider){

        $stateProvider.state('home',{
            url:'/home',
            templateUrl:'views/home.html',
            controller:'homeCtrl'
        });

        $stateProvider.state('gallery',{
            url:'/gallery',
            templateUrl:'views/gallery.html',
            controller:'galleryCtrl as user',
            resolve: {
                cards: [
                    'serviceGallery',
                    function(serviceGallery) {
                        return serviceGallery.getCardsList();
                    }
                ]
            }
        });

        $urlRouterProvider.otherwise('/home');
    })

})(angular);
