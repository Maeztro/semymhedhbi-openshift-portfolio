//express
    var express = require('express');
//cors
    var cors=require('cors');
//passport
    var passport = require('passport');
    require('passport');
    require('dotenv').config();
//parsers
    var body_parser = require('body-parser');
    var cookie_parser = require('cookie-parser');
//mongo
    var mongoose = require('mongoose');
    var cloud_uri = 'mongodb://finalboss:4b0xFu110fk1tt3ns_@portfolio-shard-00-00-vbegi.mongodb.net:27017,' +
        'portfolio-shard-00-01-vbegi.mongodb.net:27017,' +
        'portfolio-shard-00-02-vbegi.mongodb.net:27017/portfolio?ssl=true&replicaSet=Portfolio-shard-0&authSource=admin';
    //mongoose.connect('mongodb://localhost/portfolio');
    var db = mongoose.connection;
    mongoose.connect(cloud_uri);
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
    });
//set app
    var app = express();
//set routes
    var routes = require('./app_portfolio/routes/index');
//middleware
    app.use(cors());
    app.use(body_parser.json());
    app.use(body_parser.urlencoded({extended:false}));
    app.use(cookie_parser());
//public www folder
    app.use(express.static("../www"));
    app.use('/www', express.static(__dirname + '/www'));
// passport init
    app.use(passport.initialize());
    app.use(passport.session());
//routes
    app.use('/', routes);
//error handling
    app.use(function(err,req,res,next){
        if(err.name==='UnauthorizedError'){
            res.status('401');
            res.json({"error":err.name+" : "+err.message});
        }
    });

    app.use(function(err,req,res,next){
        res.status(404).send({error:err.message});
    });
//ports
    app.listen(3000);
    //console.log('server started on port 3000');





