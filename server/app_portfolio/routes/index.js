(function () {
    // "use strict";

    var express = require('express');
    var router = express.Router();

    //auth
    var jwt=require('express-jwt'),
    auth=jwt({
        secret:process.env.JWT_SECRET,
        userProperty:'payload'
    });
    var ctrlAuth=require('../controllers/userController');
    var ctrlPortfolio=require('../controllers/portfolioController');

    //model
    var Portfolio = require('../models/portfolio');
    var User = require('../models/users');

    //USERS---------------------------------------------------------------------------------------------------------

    //login
    router.post('/api/users/login',ctrlAuth.login);

    //create new user
    router.post('/api/users/register',ctrlAuth.createNewUser);

    //todo: logout function

    module.exports = router;

    //PORTFOLIO---------------------------------------------------------------------------------------------------------

    //-home
    router.get('/',function(req,res){
        res.render('index');
    });

    //GET OPERATIONS - PORTFOLIO

    //get all portfolios
    router.get('/api/portfolio',ctrlPortfolio.getItems);
    //get one portfoly by id
    router.get('/api/portfolio/:_id',ctrlPortfolio.getItemById);

    //CRUD OPERATIONS - PORTFOLIO

    router.post('/api/portfolio',auth,ctrlPortfolio.addItem);

    router.put('/api/portfolio/:_id',auth,ctrlPortfolio.updateItem);

    router.delete('/api/portfolio/:_id',auth,ctrlPortfolio.deleteItem);

    //wow much clean !

})();