var passport=require('passport'),
    LocalStrategy=require('passport-local').Strategy;

var User = require('../models/users');

passport.use(new LocalStrategy({
        usernameField: 'username'
    },
    function(username, password, done,next) {
        User.getUserByUsername(username, function(username){
        }).then(function(resp){
            if(User.comparePassword(password, resp.password)){
                return done(null,{username:username});
            }else{
                return done({error:'invalid password'});
            }
        },function(err){
            return done({error:'invalid username' + err});
        }).catch(next);
    }
));